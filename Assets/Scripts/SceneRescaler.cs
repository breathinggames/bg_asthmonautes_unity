﻿using UnityEngine;

public class SceneRescaler : MonoBehaviour
{
    [SerializeField] float scaleBase = 1;   
    [SerializeField] float mobileScaleModifier = 0;
    [SerializeField] Vector2 posAdjustment;
    

    private float defaultScale = 1;
    private Vector2 initialPos;
    private const float FULLHD_LEVEL_WIDTH = 17.8f;

    bool changeRes = false;

    private void Start()
    {
        defaultScale = scaleBase;
        initialPos = transform.position;
    }

    void Update()
    {
        if (Screen.width <= 600 && !changeRes)
        {
            scaleBase += mobileScaleModifier;
            transform.position = posAdjustment;
            changeRes = true;
        }

        if (Screen.width > 600 && changeRes)
        {
            scaleBase = defaultScale;
            transform.position = initialPos;
            changeRes = false;
        }
        
        // get the screen height & width in world space units
        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = (worldScreenHeight / Screen.height) * Screen.width;

        Debug.Log($"worldScreenWidth {worldScreenWidth}");
        Debug.Log($"worldScreenHeight {worldScreenHeight}");

        Vector2 scaleResult = Vector2.one;
        scaleResult.x = worldScreenWidth / FULLHD_LEVEL_WIDTH;
        scaleResult.y = scaleResult.x;

        transform.localScale = scaleResult * scaleBase;

        Debug.Log($"Scale {scaleResult}");
    }
}
