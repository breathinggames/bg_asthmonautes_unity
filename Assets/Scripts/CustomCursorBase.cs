﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CustomCursorBase : MonoBehaviour
{
    public Color cursorColor;
    public ParticleSystem clickEffect;
    public GameObject trailEffect;
    public AudioClip[] audioData;
    public Sprite baseCursor;
    public Sprite selectionCursor;

    protected int i = 0;
    protected SpriteRenderer sRend;
    protected AudioSource targetAudioSource;
    protected Animator animState;
    protected Vector2 cursorPos;
    protected bool isInUILayout;

    void Start()
    {
        Cursor.visible = false;
        sRend = GetComponent<SpriteRenderer>();
        animState = GetComponent<Animator>();
        clickEffect.GetComponent<ParticleSystem>();
        trailEffect.GetComponent<GameObject>();
        targetAudioSource = GetComponent<AudioSource>();       
    }

    void Update()
    {
        cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = cursorPos;

        if (EventSystem.current.IsPointerOverGameObject())
        {
            isInUILayout = true;
        }
        else
        {
            isInUILayout = false;
        }

        OnDefaultCur(cursorPos);

        #if UNITY_ANDROID || UNITY_IOS           
        #else
            ChangeCursorAnim();
        #endif

        Cursor.visible = false;
    }

    protected virtual void OnDefaultCur(Vector2 cursorPos)
    {
        if (Input.GetMouseButtonDown(0))
        {
            Collider2D hit = Physics2D.OverlapPoint(cursorPos);
            clickEffect.transform.position = cursorPos;

            RaycastHitOnClick(hit);
        }
        else if (Input.GetMouseButton(0))
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToSelectionCursor();
            #endif
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (!isInUILayout)
            {
                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToBaseCursor();
                #endif
            }            
        }
        else
        {
            if (!isInUILayout)
            {
                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToBaseCursor();
                #endif
            }            
        }
    }

    protected virtual void RaycastHitOnClick(Collider2D hit)
    {
        if (hit)
        {
            Debug.Log("Hit" + hit.transform.name);
        }
        else
        {
            PlayRandomClickSound();

            Debug.Log(hit);
            clickEffect.Play();
        }
    }

    protected virtual void PlayRandomClickSound()
    {
        if (i == 8) { i = 0; }

        targetAudioSource.clip = audioData[i];
        targetAudioSource.Play();
        i++;
    }

    public virtual void ChangeCursorToAndroidCursor()
    {
        sRend.color = new Color(1, 1, 1, 0);
    }

    public virtual void ChangeCursorToBaseCursor()
    {
        sRend.sprite = baseCursor;
        trailEffect.SetActive(true);
        sRend.color = new Color(1, 1, 1, 1);
    }

    public virtual void ChangeCursorToSelectionCursor()
    {
        sRend.sprite = selectionCursor;
        trailEffect.SetActive(true);
        sRend.color = new Color(1, 1, 1, 1);
    }

    public void ChangeCursorAnim()
    {
        if (Screen.width <= 1000)
        {
            animState.SetBool("IsMobile", true);
        }
        else
        {
            animState.SetBool("IsMobile", false);
        }
    }
}
