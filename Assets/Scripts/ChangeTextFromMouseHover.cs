﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTextFromMouseHover : Clickable2D
{
    string objectName;

    public SpriteRenderer mouseRend;
    public Sprite mouseCustomBase;
    public Sprite mouseCustomSelection;

    private void Start()
    {
        mouseRend.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        objectName = this.name;
    }

    protected override void ChangeCursor(Texture2D cursorTexture)
    {
        if (!clickEnabled)
        {
            return;
        }

        #if UNITY_ANDROID || UNITY_IOS
            mouseRend.color = new Color(1, 1, 1, 0);
        #else
            mouseRend.sprite = mouseCustomSelection;
        #endif

        Flowchart.BroadcastFungusMessage(objectName + " Menu Hover");
    }

    protected override void DoPointerExit()
    {
        #if UNITY_ANDROID || UNITY_IOS
            mouseRend.color = new Color(1, 1, 1, 0);
        #else
            mouseRend.sprite = mouseCustomBase;
        #endif

        Flowchart.BroadcastFungusMessage(objectName + " Menu Exit");
    }
}
