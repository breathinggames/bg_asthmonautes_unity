﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorHoverInventory : MonoBehaviour
{
    public bool isInInventoryLayout;

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            isInInventoryLayout = true;
        }
        else
        {
            isInInventoryLayout = false;
        }
    }
}
