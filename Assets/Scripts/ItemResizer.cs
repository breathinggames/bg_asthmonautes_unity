﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemResizer : MonoBehaviour
{

    public float xScale = 56;
    public float yScale = 32;

    void Start()
    {
        if (Screen.width <= 600)
        {
            transform.localScale = new Vector2(xScale, yScale);
        }
    }
}
