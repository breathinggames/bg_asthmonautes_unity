﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CustomCursorExtended : CustomCursorBase
{
    public Sprite inventoryBaseCursor;
    public Sprite inventorySelectionCursor;
    public Sprite dragBaseCursor;
    public Sprite dragHoverTargetCursor;

    public bool isDraggingItem = false;
    public bool isHoverNPC = false;
    public bool isHoverInventoryNPC = false;
    public bool isDraggingAndHoverInventoryNPC = false;

    private void Update()
    {
        cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = cursorPos;

        if (EventSystem.current.IsPointerOverGameObject())
        {
            isInUILayout = true;
        }
        else
        {
            isInUILayout = false;
        }

        if (isInUILayout && !isHoverInventoryNPC)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Collider2D hit = Physics2D.OverlapPoint(cursorPos);
                clickEffect.transform.position = cursorPos;

                RaycastHitOnClick(hit);
            }
            else if (Input.GetMouseButton(0) && isHoverInventoryNPC)
            {
                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToDragBaseCursor();
                #endif
            }
            else if (Input.GetMouseButtonUp(0))
            {
                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToBaseInventoryCursor();
                #endif
            }
            else
            {
                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToBaseInventoryCursor();
                #endif
            }
        }
        else if (isInUILayout && isDraggingAndHoverInventoryNPC && isHoverInventoryNPC)
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToDragHoverTargetCursor();
            #endif
        }
        else if (isInUILayout && isHoverInventoryNPC && !isDraggingAndHoverInventoryNPC)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Collider2D hit = Physics2D.OverlapPoint(cursorPos);
                clickEffect.transform.position = cursorPos;

                RaycastHitOnClick(hit);
            }
            else if (Input.GetMouseButton(0))
            {
                isDraggingItem = true;

                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToDragBaseCursor();
                #endif
            }
            else if (Input.GetMouseButtonUp(0))
            {
                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToBaseInventoryCursor();
                #endif
            }
            else
            {
                #if UNITY_ANDROID || UNITY_IOS
                    ChangeCursorToAndroidCursor();
                #else
                    ChangeCursorToSelectionInventoryCursor();
                #endif
            }
        }
        else if (!isInUILayout && isDraggingItem && !isHoverNPC)
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToDragBaseCursor();
            #endif
        }
        else if (!isInUILayout && isDraggingItem && isHoverNPC)
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToDragHoverTargetCursor();
            #endif
        }
        else if (!isInUILayout && isHoverNPC)
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToSelectionCursor();
            #endif
        }
        else
        {
            OnDefaultCur(cursorPos);
        }

        #if UNITY_ANDROID || UNITY_IOS
        #else
            ChangeCursorAnim();
        #endif
    
        Cursor.visible = false;
    }

    protected override void OnDefaultCur(Vector2 cursorPos)
    {
        if (Input.GetMouseButtonDown(0))
        {
            Collider2D hit = Physics2D.OverlapPoint(cursorPos);
            clickEffect.transform.position = cursorPos;

            RaycastHitOnClick(hit);
        }
        else if (Input.GetMouseButton(0))
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToSelectionCursor();
            #endif
        }
        else if (Input.GetMouseButtonUp(0))
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToBaseCursor();
            #endif
        }
        else
        {
            #if UNITY_ANDROID || UNITY_IOS
                ChangeCursorToAndroidCursor();
            #else
                ChangeCursorToBaseCursor();
            #endif
        }
    }

    private void ChangeCursorToBaseInventoryCursor()
    {
        sRend.sprite = inventoryBaseCursor;
        trailEffect.SetActive(true);
        sRend.color = new Color(1, 1, 1, 1);
    }

    private void ChangeCursorToDragBaseCursor()
    {
        sRend.sprite = dragBaseCursor;
        trailEffect.SetActive(false);
        sRend.color = cursorColor;
    }

    private void ChangeCursorToDragHoverTargetCursor()
    {
        sRend.sprite = dragHoverTargetCursor;
    }

    private void ChangeCursorToSelectionInventoryCursor()
    {
        sRend.sprite = inventorySelectionCursor;
        trailEffect.SetActive(true);
        sRend.color = new Color(1, 1, 1, 0.3f);
    }

    public void SecurityResetBool()
    {
        isDraggingItem = false;
        isHoverNPC = false;
        isHoverInventoryNPC = false;
        isDraggingAndHoverInventoryNPC = false;
    }
}
