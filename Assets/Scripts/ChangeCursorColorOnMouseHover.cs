﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCursorColorOnMouseHover : Draggable2D
{
    public SpriteRenderer mouseRend;
    public Sprite inventoryMouseCustomBase;
    public Sprite inventoryMouseCustomSelection;
    public Sprite DragHoverNPC;
    public CustomCursorExtended cc;
    public GameObject trailEffect;

    void Start()
    {
        mouseRend.GetComponent<SpriteRenderer>();
        cc.GetComponent<CustomCursorExtended>();
        trailEffect.GetComponent<GameObject>();
    }

    protected override void ChangeCursor(Texture2D cursorTexture)
    {
        if (!dragEnabled)
        {
            return;
        }

        if (!cc.isDraggingItem)
        {           
            cc.isHoverInventoryNPC = true;

#if UNITY_ANDROID || UNITY_IOS
                mouseRend.color = new Color(1, 1, 1, 0);
#else
            mouseRend.sprite = inventoryMouseCustomSelection;
#endif

        }        
    }

    protected override void DoPointerExit()
    {
        if (!cc.isDraggingItem)
        {
            cc.isHoverInventoryNPC = false;

#if UNITY_ANDROID || UNITY_IOS
                mouseRend.color = new Color(1, 1, 1, 0);
#else
            mouseRend.sprite = inventoryMouseCustomBase;
#endif
            
        }
    }

    protected override void DoBeginDrag()
    {      
        base.DoBeginDrag();
        trailEffect.SetActive(true);
    }

    protected override void DoEndDrag()
    {
        base.DoEndDrag();
        
        cc.isDraggingItem = false;
        StartCoroutine(DisableEffect());
    }

    IEnumerator DisableEffect()
    {
        yield return new WaitForSeconds(0.4f);
        trailEffect.SetActive(false);
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        cc.isDraggingAndHoverInventoryNPC = true;
    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        base.OnTriggerExit2D(other);
        cc.isDraggingAndHoverInventoryNPC = false;
    }
}
