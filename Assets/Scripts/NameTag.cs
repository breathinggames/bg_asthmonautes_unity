﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameTag : MonoBehaviour
{
    [SerializeField] private GameObject characterName;

    private void Awake()
    {
        characterName.SetActive(false);
    }

    private void OnMouseOver()
    {
        characterName.SetActive(true);
    }

    private void OnMouseExit()
    {
        characterName.SetActive(false);
    }
}
