﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseControl : MonoBehaviour
{
    public SpriteRenderer mouseRend;

    private void Start()
    {
        mouseRend.GetComponent<SpriteRenderer>();
    }

    public void MouseActivator()
    {
        Cursor.visible = true;
        mouseRend.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    public void MouseDeactivator()
    {
        Cursor.visible = false;
        mouseRend.gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }
}
