﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandOscillator : MonoBehaviour
{
    public float speed;
    public float sinMov;
    public float cosMov;

    float oscillator;
    float initX;
    float initY;

    private void Start()
    {
        initX = transform.position.x;
        initY = transform.position.y;
    }

    void Update()
    {
        oscillator += speed * Time.deltaTime;

        float x = Mathf.Cos(oscillator) * sinMov;
        float y = Mathf.Sin(oscillator) * cosMov;

        transform.position = new Vector2(x + initX, y + initY);
    }
}
