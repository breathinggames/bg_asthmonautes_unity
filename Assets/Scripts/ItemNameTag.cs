﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemNameTag : MonoBehaviour
{
    [SerializeField] private GameObject characterName;

    private void Awake()
    {
        characterName.SetActive(false);
    }

    private void Update()
    {
        
    }

    private void OnMouseOver()
    {
        characterName.SetActive(true);
    }

    private void OnMouseEnter()
    {
        characterName.SetActive(true);
    }

    private void OnMouseExit()
    {
       characterName.SetActive(false);
    }
}
