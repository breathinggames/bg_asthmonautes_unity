﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryResizer : MonoBehaviour
{
    public RectTransform rt; 
    
    void Start()
    {
        if (Screen.width <= 600)
        {
            GetComponent<GridLayoutGroup>().constraintCount = 2;
            GetComponent<GridLayoutGroup>().cellSize = new Vector2(150, 150);
            GetComponent<GridLayoutGroup>().spacing = new Vector2(90, 50);

            rt.GetComponent<RectTransform>().sizeDelta = new Vector2(820, 450);
            transform.localPosition = new Vector2(-1000, 415);
        }
    }
}
