﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundsFromMouseHover : Clickable2D
{
    [SerializeField] AudioClip[] audioData;
    [SerializeField] AudioSource targetAudioSource;

    public SpriteRenderer mouseRend;
    public Sprite mouseCustomBase;
    public Sprite mouseCustomSelection;

    string objectName;
    public CustomCursorExtended cc;

    void Start()
    {
        targetAudioSource = GetComponent<AudioSource>();
        mouseRend.GetComponent<SpriteRenderer>();
        cc.GetComponent<CustomCursorExtended>();
    }

    private void Update()
    {
        objectName = this.name;
    }

    protected override void ChangeCursor(Texture2D cursorTexture)
    {
        if (!clickEnabled)
        {
            return;
        }

        cc.isHoverNPC = true;

        if (cc.isDraggingItem == false)
        {
            #if UNITY_ANDROID || UNITY_IOS
                mouseRend.color = new Color(1, 1, 1, 0);
            #else
                mouseRend.sprite = mouseCustomSelection;
            #endif
        }

        targetAudioSource.clip = audioData[0];
        targetAudioSource.Play();

        Flowchart.BroadcastFungusMessage(objectName + " Start");
    }

    protected override void DoPointerExit()
    {
        cc.isHoverNPC = false;

        if (cc.isDraggingItem == false)
        {
            #if UNITY_ANDROID || UNITY_IOS
                mouseRend.color = new Color(1, 1, 1, 0);
            #else
                mouseRend.sprite = mouseCustomBase;
            #endif
        }

        targetAudioSource.clip = audioData[1];
        targetAudioSource.Play();

        Flowchart.BroadcastFungusMessage(objectName + " End");
    }
}
