﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSprite : MonoBehaviour
{
    public float speed;
    float warpEffectPositive;
    float warpEffectNegative;
    Vector2 pos;

    public bool isReverted;

    public GameObject mainCam;

    private void Start()
    {
        warpEffectPositive = 11;
        warpEffectNegative = -11;

        pos.x = transform.position.x;
        pos.y = transform.position.y;

        mainCam.GetComponent<Transform>();
    }

    void Update()
    { 
        if (isReverted)
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
            if (transform.position.x <= mainCam.transform.position.x - 11)
            {
                pos.x = mainCam.transform.position.x + 11;
                transform.position = pos;
            }
            else
            {
                transform.Translate(Vector2.left * speed);
            }
        }
        else
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            if (transform.position.x >= mainCam.transform.position.x + 11)
            {
                pos.x = mainCam.transform.position.x - 11;
                transform.position = pos;
            }
            else
            {
                transform.Translate(Vector2.right * speed);
            }
        }
    }
}
