﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRepositionner : MonoBehaviour
{
    public float xRepos;
    public float yRepos;
    
    void Start()
    {
        if (Screen.width <= 600)
        {
            GetComponent<RectTransform>().anchoredPosition = new Vector2(xRepos, yRepos);
        }        
    }
}
