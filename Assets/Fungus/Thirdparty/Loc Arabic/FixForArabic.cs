﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FixForArabic : MonoBehaviour
{
    public bool IsArabicSetActive = false;

    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    public bool IsArabic()
    {
        return IsArabicSetActive = true;
    }

    public bool IsNotArabic()
    {
        return IsArabicSetActive = false;
    }
}
