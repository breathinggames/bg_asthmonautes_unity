﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class TextCorrectorForArabic : MonoBehaviour
{
    public string text;

    // Use this for initialization
    void Start()
    {
        gameObject.GetComponent<Text>().text = "Asthma Heroes est un jeu destiné à des fins de recherche en promotion de la santé. Il ne vise pas à diagnostiquer, traiter ou guérir une maladie. Ce n’est pas un appareil médical et ne remplace pas les conseils d’une professionnelle." + ArabicFixer.Fix(text, true, false);
    }
}
